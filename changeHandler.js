module.exports = {
  getAmount: function(coinType) {
    // COINS:
    // [p]enny
    // [n]ickel
    // [d]ime
    // [q]uarter
    const _dict = {
      p: 1,
      n: 5,
      d: 10,
      q: 25
    };
    
    const value = _dict[coinType];
    if (value) {
      return value;
    } else {
      throw new Error('Unrecognized coin ' + coinType);
    }
  },
  
  convertToChange: function(cent) {
    const coins = ['q', 'd', 'n', 'p'];
    const ret = [];
    
    while(cent != 0) {
      for(let i = 0; i < coins.length; i++) {
        const coin = coins[i];
        const coinValue = this.getAmount(coin);
        if (cent >= coinValue) {
          cent -= coinValue;
          ret.push(coin);
          break;
        }
      }
    }
    
    return ret;
  }
};
