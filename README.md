# Note

* There shouldn't be lots of things inside a single module. One should try to separate functions according to their role into separate small modules.
* Don't use `switch`. It's a 'code smell', and can be easily replaced by other means e.g. object, Map.
* Use unit test & test driven development
* Declare variable first (don't create variable implicitly)
* Use consistent quote mark ('' or "")
